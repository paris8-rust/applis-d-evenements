**Des fleurs et leur voisinage**

Il s'agit d'implémenter l'algorithme des k-plus proches voisins (kppv) à une base de données classique en apprentissage automatique : les iris. La base de données des iris est présenté ci-dessous, puis la description du travail est détaillé. Enfin, l'algorithme des kppv est rappelé.

**Classes :**
Iris setosa

Iris versicolor
	
Iris virginica

**Attributs :**

    Longueur des sépales
    Largeur des sépales
    Longueur des pétales
    Largeur des pétales

** SÉPALE**, subst. masc.

BOT. Chacun des éléments foliacés, généralement verts, dont la réunion compose le calice et supporte la corolle de la fleur.

**PÉTALE**, subst. masc.

A. − BOT. Chacun des éléments foliacés, généralement colorés, qui composent la corolle d'une fleur.


_**Apprentissage du meilleur k pour la classification des iris, à l’aide de l’algorithme des k-plus proches voisins (k-ppv).**_

Le programme devra :

**1** - Prendre en entrée quatre fichiers :

- iris_learn_data

- iris_learn_label

- iris_test_data

- iris_test_label

Les deux fichiers data auront le même format, une donnée par ligne sous la forme :

5.0;3.3;1.4;0.2;

7.0;3.2;4.7;1.4;

6.5;3.2;5.1;2.0;

Les deux fichiers label auront le même nombre de lignes que le fichier data correspondant, chaque ligne indiquant la classe de la ligne correspondante dans le fichier data. Avec :

- 0 désigne la classe des iris setosa

- 1 désigne la classe des iris versicolor

- 2 désigne la classe des iris virginica

 **2** - Calculer la classe de chaque entrée du fichier iris_test_data, en appliquant les k-ppv.

** 3 **- Calculer le taux de bonne classification, grâce à iris_test_label

** 4** - Faire varier k, de 2 à 10 et trouver le meilleur taux de classification.

** 5** - Stocker le taux de bonne classification pour chaque k.
Rappels de l’algorithme des k-plus proches voisins :

Pour attribuer une classe à un nouvel élément x (ici, à une donnée de iris_test_data) on va calculer sa distance à chacun des points de l’ensemble d’apprentissage (ici, iris_learn_data) et affecter la classe majoritaire parmi les k points les plus proches de x (la classe d’un point est la valeur de la ligne correspondante dans iris_learn_label).


Projet inspiré du cours d'intelligence artificielle.