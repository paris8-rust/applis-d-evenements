mod src_types;
use crate::src_types::Pair;
mod src_parse;
use src_parse::load_data;
mod src_kppv;
use src_kppv::*;
use std::collections::HashMap;

fn main() {
    //1 - Prendre en entrée quatre fichiers :
    //iris_learn_data avec iris_learn_label
    //iris_test_data avec iris_test_label
    let file_learn_data = "iris_learn_data";
    let file_learn_label = "iris_learn_label";
    let file_test_data = "iris_test_data";
    let file_test_label = "iris_test_label";
    let hash_distance: HashMap<usize, Vec<Pair>>;
    let vec_perf: Vec<f32>;

    // Charger les données
    let model = match load_data(file_learn_data.to_string(), file_learn_label.to_string()) {
        Ok(f) => f,
        Err(e) => panic!(e),
    };

    let test = match load_data(file_test_data.to_string(), file_test_label.to_string()) {
        Ok(f) => f,
        Err(e) => panic!(e),
    };

    // 2 - Calculer la classe de chaque entrée du fichier iris_test_data,
    // en appliquant les k-ppv.
    hash_distance = calc_dist_kppv(&test, &model);

    //3 - Calculer le taux de bonne classification, grâce à iris_test_label
    //4 - Faire varier k, de 2 à 10 et trouver le meilleur taux de classification.
    //5 - Stocker le taux de bonne classification pour chaque k.
    vec_perf = calcul_category(&test, 10, hash_distance, &model);
    affiche_les_performances_kppv(vec_perf);
}
