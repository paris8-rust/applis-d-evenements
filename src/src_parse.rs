use crate::src_types::{parse_category, Category, Iris};
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};

//Permet de lire les fichiers de données et de classification des iris
pub fn load_data(file_data: String, file_label: String) -> Result<Vec<Iris>, io::Error> {
    let mut set_learn: Vec<Iris> = Vec::new();
    let fd_data = match File::open(file_data) {
        Ok(f) => f,
        Err(e) => {
            println!(
                "Erreur d'ouverture du fichier de données d'apprentissage : {} ",
                e
            );
            return Err(e);
        }
    };
    let fd_label = match File::open(file_label) {
        Ok(f) => f,
        Err(e) => {
            println!(
                "Erreur d'ouverture du fichier les classes des données d'apprentissage : {} ",
                e
            );
            return Err(e);
        }
    };
    let reader_data = BufReader::new(fd_data);
    let reader_label = BufReader::new(fd_label);

    for line in reader_data.lines() {
        let line = line.unwrap();
        //Lecture et convertion des strings en float
        let strings: Vec<_> = line.split(";").collect();
        let numbers: Vec<f32> = strings.iter().flat_map(|x| x.parse()).collect();
        //println!("APRES SPLIT {:?}",numbers);
        let current_iris = Iris::new(numbers, Category::Setosa);
        set_learn.push(current_iris);
    }
    let mut i = 0;
    for line in reader_label.lines() {
        let line = line.unwrap();
        let chaine: Vec<&str> = line.split(";").collect();
        let current_category = match parse_category(chaine[0]) {
            Ok(val) => val,
            Err(error) => panic!("Probleme de catégorie : {:?}", error),
        };
        //println!("Vue category :  {:?}",current_category);
        let mut obj = &mut set_learn[i];
        obj.iris_class = current_category;
        i += 1;
    }
    return Ok(set_learn);
}
