use crate::src_types::{Category, Iris, Pair};
use std::collections::HashMap;

//Fonction qui trie un vecteur de Pair en fonctioin de la distance la plus petite.
pub fn sort_set_pair(sp: &mut Vec<Pair>) -> Vec<Pair> {
    sp.sort_by(|a, b| a.dist.partial_cmp(&b.dist).unwrap());
    //println!("{:?}",sp);
    return sp.to_vec();
}
// Affiche les elements d'un vecteur.
pub fn affiche_les_performances_kppv(vec_perf: Vec<f32>) {
    for (i, elt) in vec_perf.iter().enumerate() {
        println!(
            "Pour {} voisins, le taux de succès est de  {} %",
            i + 2,
            elt
        );
    }
}
// Transforme un vecteur du type Iris en un vecteur de type Category
pub fn create_vec_attendue(set_test: &Vec<Iris>) -> Vec<Category> {
    let mut vec_attendue: Vec<Category> = Vec::new();
    for elt in set_test {
        vec_attendue.push(elt.iris_class);
    }
    return vec_attendue;
}
//Permet de calculer les k voisins pour chaque element
// Renvoie un vecteur ayant la performance des reussite
pub fn calcul_category(
    set_test: &Vec<Iris>,
    k: usize,
    hash_dist: HashMap<usize, Vec<Pair>>,
    set_model: &Vec<Iris>,
) -> Vec<f32> {
    let _n: usize;
    let mut sub_dist = Vec::new();
    let mut vote_maj: Category;
    let vec_attendue: Vec<Category>;
    let mut res: f32;
    let mut vec_res = Vec::new();

    vec_attendue = create_vec_attendue(&set_test);
    for n in 2..k + 1 {
        let mut vec_obtenue: Vec<Category> = Vec::new();
        println!(" pour n {}", n);
        for (key, _elt) in set_test.iter().enumerate() {
            //println!("i {}",key);
            //println!("{:?}", elt);
            match hash_dist.get(&key) {
                Some(value) => {
                    //println!("value {:?}", value);
                    sub_dist = value[0..n].to_vec();
                }
                None => println!("{} is unreviewed.", key),
            };
            //Vote majoritaire pour pour le nieme test au kieme
            //println!("Pour k : {} , key : {} ",n,key);
            vote_maj = get_learn_category(&sub_dist, &set_model);
            //println!("Vote maj : {}",vote_maj);
            vec_obtenue.push(vote_maj);
        }
        //On a le vecteur pour un kdonnee de tout element de test
        res = compare_results(&vec_attendue, &vec_obtenue);
        println!("% : {}\n", res);
        vec_res.push(res);
    }
    return vec_res;
}
// Fonction identifie les catégories trouvés grace à son id
// Renvoie la classe majoritaire des catégorie trouvés.
pub fn get_learn_category(sub_pair: &Vec<Pair>, model: &Vec<Iris>) -> Category {
    let mut vec_cat: Vec<Category> = Vec::new();
    let vm: Category;
    //println!("learn category {:?}",sub_pair);
    for elt in sub_pair {
        vec_cat.push(model[elt.id].iris_class);
    }
    //println!("Valeur possible {:?}",vec_cat);
    vm = vote_majoritaire(vec_cat);
    //println!("Valeur choisit {}",vm);
    return vm;
}
//Indique la catégorie majoritaire présente dans un vecteur de catégorie
//Si une catégorie est présente autant qu'une autre alors on prend une d'elle aléatoirement
// La hashmap est désordonnée donc elle est créer de l'aléatoire
pub fn vote_majoritaire(vect: Vec<Category>) -> Category {
    let mut hash_vote = HashMap::new();
    let mut max = -1;
    let mut ident: Category = Category::Setosa;
    for elt in vect {
        let cnt = hash_vote.entry(elt).or_insert(0);
        *cnt += 1;
    }
    for (id, value) in &hash_vote {
        if value > &max {
            max = *value;
            ident = *id;
            //println!("****in  BOUCLE****{} chemin : {:?}", ident,max);
        }
    }
    //println!("****PLUS GRAND fin BOUCLE****{} chemin : {:?}", ident,max);
    return ident;
}
// Compare les resultats des catégories obtenues avec les catégories attendues
//Renvoie un pourcentages de reussite
pub fn compare_results(attendue: &Vec<Category>, obtenue: &Vec<Category>) -> f32 {
    let iter = attendue.iter().zip(obtenue.iter());
    let mut res = 0.0;
    let long = attendue.len() as f32;
    println!("Vec obtenue {:?}", obtenue);
    println!("Vec attendue {:?}", attendue);
    for (att, obt) in iter {
        if att == obt {
            res += 1.0;
        }
    }
    return (res / long) * 100.0;
}
//Fonction de calcul de la distance euclidienne entre deux points
pub fn calcul_distance_euclidienne(x: &Vec<f32>, y: &Vec<f32>) -> f32 {
    let mut res: f32 = 0.0;
    for i in (std::ops::Range {
        start: 0,
        end: x.len(),
    }) {
        res += (x[i] - y[i]).powi(2);
    }
    return res.sqrt();
}
//Permet de calculer les kppv pour chaque element test.
// Renvoie une hashmap avec pour chaque element ces voisins les plus proche dans un vecteur trié.
pub fn calc_dist_kppv(test: &Vec<Iris>, model: &Vec<Iris>) -> HashMap<usize, Vec<Pair>> {
    let mut hash_elt_kppv = HashMap::new();
    for (i, elt) in test.iter().enumerate() {
        //println!("calc_dist_kppv i: {}",i);
        let mut dist_elt = calcul_dist_elt_kppv(elt, model);
        sort_set_pair(&mut dist_elt);
        //println!("{:?}",dist_elt);
        //println!("Nous devons trier la liste avant de la mémoriser");
        hash_elt_kppv.insert(i, dist_elt);
        /*
        for (id, dist) in &hash_elt_kppv {
            println!("**********{} chemin : {:?}", id,dist);
        }*/
    }
    return hash_elt_kppv;
}
// Calcul la distance euclidienne de tout les element learn data avec un element test
//Renvoie un vecteur ayant la distance et index de l'element calculer issus du model
pub fn calcul_dist_elt_kppv(elt: &Iris, model: &Vec<Iris>) -> Vec<Pair> {
    let mut dist_elt: Vec<Pair> = Vec::new();
    for (i, entre) in model.iter().enumerate() {
        //println!("calcul_dist_elt_kppv i   {}",i);
        let dist = calcul_distance_euclidienne(&elt.iris_data, &entre.iris_data);
        let pair = Pair::new(dist, i);
        //println!("Voici la pair: dist - {}  index- {}",pair.dist,pair.id);
        dist_elt.push(pair);
    }
    return dist_elt;
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_distance_euclidienne() {
        assert_eq!(
            calcul_distance_euclidienne(&vec![4.0, 8.0], &vec![1.0, 4.0]),
            5.0
        );
    }
    #[test]
    fn test_compare_results() {
        assert_eq!(
            compare_results(
                &vec![Category::Setosa, Category::Versicolor, Category::Virginica],
                &vec![Category::Setosa, Category::Versicolor, Category::Virginica]
            ),
            100.0
        );
        assert_eq!(
            compare_results(
                &vec![Category::Setosa, Category::Setosa, Category::Virginica],
                &vec![Category::Setosa, Category::Versicolor, Category::Virginica]
            ),
            66.66667
        );
    }
    #[test]
    fn test_vote_majoritaire() {
        assert_eq!(
            vote_majoritaire(vec![
                Category::Setosa,
                Category::Setosa,
                Category::Versicolor,
                Category::Versicolor,
                Category::Versicolor,
                Category::Virginica
            ]),
            Category::Versicolor
        );
        assert_eq!(
            vote_majoritaire(vec![
                Category::Setosa,
                Category::Versicolor,
                Category::Setosa,
                Category::Virginica
            ]),
            Category::Setosa
        );
    }
    #[test]
    fn test_sort_set_pair() {
        assert_eq!(
            sort_set_pair(&mut vec![
                Pair::new(0.31, 1),
                Pair::new(0.1, 2),
                Pair::new(0.2, 3),
            ]),
            [
                Pair { dist: 0.1, id: 2 },
                Pair { dist: 0.2, id: 3 },
                Pair { dist: 0.31, id: 1 }
            ]
        );
    }
    #[test]
    fn test_create_vec_attendue() {
        assert_eq!(
            create_vec_attendue(&vec![
                Iris::new(vec![5.8, 2.8, 5.1, 2.4], Category::Virginica),
                Iris::new(vec![6.0, 2.2, 4.0, 1.0], Category::Versicolor)
            ]),
            vec![Category::Virginica, Category::Versicolor]
        );
    }
    #[test]
    fn test_get_learn_category() {
        assert_eq!(
            get_learn_category(
                &vec![Pair::new(0.31, 0), Pair::new(0.1, 1),],
                &vec![
                    Iris::new(vec![5.8, 2.8, 5.1, 2.4], Category::Setosa),
                    Iris::new(vec![6.0, 2.2, 4.0, 1.0], Category::Setosa)
                ]
            ),
            Category::Setosa
        );
    }
    #[test]
    fn test_calcul_dist_elt_kppv() {
        assert_eq!(
            calcul_dist_elt_kppv(
                &Iris::new(vec![4.0, 8.0], Category::Virginica),
                &vec![
                    Iris::new(vec![1.0, 4.0], Category::Versicolor),
                    Iris::new(vec![1.0, 4.0], Category::Versicolor)
                ]
            ),
            vec![Pair { dist: 5.0, id: 0 }, Pair { dist: 5.0, id: 1 }]
        );
    }
}
