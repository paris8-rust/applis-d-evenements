use std::fmt;
use std::fmt::Debug;

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash)]
// Enumération des différentes classes issus des fichiers labels.
pub enum Category {
    //- 0 désigne la classe des iris setosa
    //- 1 désigne la classe des iris versicolor
    //- 2 désigne la classe des iris virginica
    Setosa,
    Versicolor,
    Virginica,
}
//Permet l'affichage de la catégorie
impl std::fmt::Display for Category {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let i = match self {
            Category::Setosa => "0 - Setosa",
            Category::Versicolor => "1 - Versicolor",
            Category::Virginica => "2 - Virginica",
        };
        write!(f, "{}", i)
    }
}
//Fonction qui permet de transformer un caractere en une Category
#[derive(Debug)]
pub struct ErrorInvalidCategory;
pub fn parse_category(instr: &str) -> Result<Category, ErrorInvalidCategory> {
    match instr {
        "0" => Ok(Category::Setosa),
        "1" => Ok(Category::Versicolor),
        "2" => Ok(Category::Virginica),
        _ => Err(ErrorInvalidCategory),
    }
}
#[derive(Debug)]
//Structure Iris qui représente les mesures de la fleur et sa classification
pub struct Iris {
    pub iris_data: Vec<f32>,
    pub iris_class: Category,
}
impl Iris {
    // Création d'une iris avec les valeurs d'initialisation
    pub fn new(iris_data: Vec<f32>, iris_class: Category) -> Iris {
        Iris {
            iris_data,
            iris_class,
        }
    }
}
//Affichage de la structure iris
impl std::fmt::Display for Iris {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Iris - iris_data : {:?} - iris_class :{:?} ",
            self.iris_data, self.iris_class
        )
    }
}
#[derive(Debug, PartialEq, Copy, Clone)]
//Structure ayant la distance entre deux element issus learn data et test data et id permeet identifier elt learn associer
pub struct Pair {
    pub dist: f32,
    pub id: usize,
}
impl Pair {
    // Création d'une Pair avec les valeurs d'initialisation
    pub fn new(dist: f32, id: usize) -> Pair {
        Pair { dist, id }
    }
}
//Permet l'affichage de la Pair
impl std::fmt::Display for Pair {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Pair - dist : {} - id :{} ", self.dist, self.id)
    }
}
